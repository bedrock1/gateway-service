const { ApolloServer, gql } = require("apollo-server");
const { buildFederatedSchema } = require("@apollo/federation");
const fetch = require("node-fetch");

const port = 4003;
const apiUrl = "http://localhost:8003";

const typeDefs = gql`
  type Cart @key(fields: "id") {
    id: ID!
    cartId: String
    customerId: String
    productIds: [Product]
    creationDate: String
    lastModifiedDate: String  
  } 

  extend type Product @key(fields: "id") {
    id: ID! @external    
  }

  extend type Query {
    cart(id: ID!): Cart
    carts: [Cart]
  }
`;

const resolvers = { 
  Cart: {
    productIds(cart) {
      return cart.productIds.map(id =>({ __typename: "Product", id}));
    }
  },  
  Query: {
    cart(_, { id }) {
      return fetch(`${apiUrl}/carts/${id}`).then(res => res.json());
    },
    carts() {
      return fetch(`${apiUrl}/carts/`).then(res => res.json());
    }
  }
};

const server = new ApolloServer({
  schema: buildFederatedSchema([{ typeDefs, resolvers }])
});

server.listen({ port }).then(({ url }) => {
  console.log(`Carts service ready at ${url}`);
});