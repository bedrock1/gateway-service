const { ApolloServer, gql } = require("apollo-server");
const { buildFederatedSchema } = require("@apollo/federation");
const fetch = require("node-fetch");

const port = 4001;
const apiUrl = "http://localhost:8001";

const typeDefs = gql`
  type Product @key(fields: "id") {
    id: ID!
    productId: String
    make: String    
    name: String
    code: String    
    deviceOS: String
    available: Boolean
    preOwned: Boolean
    swapEligible: Boolean
    seoName: String
    imageUrl: String
    retailPrice: Float
    discountPrice: Float
    monthlyPrice: Float    
  }
  extend type Query {
    product(id: ID!): Product
    products: [Product]
  }
`;

const resolvers = {
  Product: {
    __resolveReference(ref) {      
      return fetch(`${apiUrl}/products/${ref.id}`).then(res => res.json());
    }
  },
  Query: {
    product(_, { id }) {
      return fetch(`${apiUrl}/products/${id}`).then(res => res.json());
    },
    products() {
      return fetch(`${apiUrl}/products/`).then(res => res.json());
    }
  }
};

const server = new ApolloServer({
  schema: buildFederatedSchema([{ typeDefs, resolvers }])
});

server.listen({ port }).then(({ url }) => {
  console.log(`Products service ready at ${url}`);
});